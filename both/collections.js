Errors = new Mongo.Collection('errors');
CompletedErrors = new Mongo.Collection('completedErrors');
Meteor.startup(function () {
if (Meteor.isServer) { 
Errors.remove({});  
Errors.insert({ error: "Status 0", dateAdded: "20/08/2015 11:51", product: "CF2", assignee: "Not Assigned", status: "New" });
Errors.insert({ error: "Status 3", dateAdded: "17/08/2015 13:21", product: "MTP", assignee: "Not Assigned", status: "New" });
Errors.insert({ error: "Missing Signature Panel", dateAdded: "19/07/2015 14:11", product: "CFO", assignee: "Not Assigned", status: "New" });
Errors.insert({ error: "Unlock Case", dateAdded: "16/08/2015 09:01", product: "ELPS", assignee: "Not Assigned", status: "New" });
Errors.insert({ error: "Case Missing", dateAdded: "20/08/2015 12:51", product: "ARP", assignee: "Not Assigned", status: "New" });
if (Meteor.users.find().count() == 0){
Accounts.createUser({username: "testusername", password: "testpassword"});
}  
}
});