Template.completedErrors.helpers({
'completedErrors': function(){
return CompletedErrors.find({}, {sort: {completedDateTime: -1}});
},
liveErrorCount: function(){
return Errors.find().count();
}  
});