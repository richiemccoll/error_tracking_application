Template.home.errors = function(){
  return Errors.find();
}
Template.home.events({
"change #error-status": function (event, template){
var status = $(event.currentTarget).val();
var errorID = this._id;
var userID = Meteor.userId();
var username = Meteor.users.findOne({_id: Meteor.userId()}).username;  
console.log("error status : " + status + " the errorID is " + errorID + " and the username working on the error is " + username);
Errors.update(errorID, {$set: {status: status, username: username} });
if (status == "completed"){
var errorDetail = Errors.findOne({_id: errorID}).error;
var currentDateTime = (new Date).toTimeString();
console.log('the error is ' + errorDetail + ' the status is ' + status + "the completed time and date is " + currentDateTime);
CompletedErrors.insert({errorName: errorDetail, completedBy: username, completedDateTime: currentDateTime, completed: true});
Errors.remove(errorID);
alert("This Error has been marked as complete and has been moved out of the live queue. If this was accidental please speak to your line manager.");  
}  
}
});

Template.main.events({
  'click p': function(){
    window.confirm('Are you sure you wish to log out?');
    if (confirm){
    Meteor.logout();
    Router.go('/login');
    }
  }
})

Template.home.helpers({
completedErrorTotal: function(){
return CompletedErrors.find().count();
}
});