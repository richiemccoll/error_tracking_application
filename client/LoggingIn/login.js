Template.login.onRendered(function(){
$('.login').validate({
submitHandler: function(event){
var username = $('[name=username]').val();
var password = $('[name=password]').val();
Meteor.loginWithPassword(username, password, function(error){
if(error){
console.log(error.reason);
} else {
Router.go("/home");
}
});
}
});
});

if (Meteor.isClient){
$.validator.setDefaults({
rules: {
            password: {
                required: true,
                minlength: 6
            }
        },
messages: {
            username: "You must be a member of the Application Support team to login."
        }  
});
}