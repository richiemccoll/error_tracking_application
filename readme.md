This is an error tracking system, similar to one used in my old workplace except this has been redone in Meteor.

Once logged in a user can view a list of live errors and completed errors for manager reporting purposes.
Within the live errors they can assign themselves to an error that they have chose to pick up.
In this case, once the hypothetical error has been completed, they change the status to "completed" which moves it into the completed errors.

If you would like to see this in action: http://error_tracking_application.meteor.com/

login: testusername or testusername2 

both of these use the same password: testpassword